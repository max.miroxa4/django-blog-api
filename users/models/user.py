from django.contrib.auth.models import AbstractUser
from django.db import models

__all__ = ("User",)


class User(AbstractUser):
    """
    Override user model
    """

    email = models.EmailField(
        "email address",
        unique=True,
        error_messages={"unique": "A user with that email already exists."},
    )
    first_name = models.CharField("first name", max_length=15)
    last_name = models.CharField("last name", max_length=20)
    avatar = models.ImageField(
        "Picture", upload_to="users/avatars/", null=True, blank=True
    )
    username = models.CharField(
        "username",
        max_length=254,
        unique=True,
        help_text="Required. 254 characters or fewer. Letters, digits and @/./+/-/_ only.",
        error_messages={"unique": "A user with that username already exists."},
    )

    def __str__(self):
        return self.get_full_name() or self.get_username()
