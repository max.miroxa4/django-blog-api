from django.urls import include, path

from users.views.index import MainView

urlpatterns = [path("", MainView.as_view(), name="index")]
