from django.urls import path
from knox.views import LogoutView

from users.api_views.user import LoginAPI, UserRegistration

urlpatterns = [
    path("registration/", UserRegistration.as_view(), name="registration"),
    path("login/", LoginAPI.as_view(), name="login"),
    path("logout/", LogoutView.as_view(), name="logout"),
]
