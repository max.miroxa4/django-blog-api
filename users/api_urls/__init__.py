from django.urls import include, path

app_name = "api-users"

urlpatterns = [
    path("user/", include("users.api_urls.user")),
    path("profile/", include("users.api_urls.profile")),
]
