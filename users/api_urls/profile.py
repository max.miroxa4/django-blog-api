from django.urls import path

from users.api_views.profile import UserUpdateDetailView

urlpatterns = [path("", UserUpdateDetailView.as_view(), name="profile")]
