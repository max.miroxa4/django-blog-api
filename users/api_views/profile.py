from rest_framework.generics import RetrieveAPIView, UpdateAPIView
from rest_framework.permissions import IsAuthenticated

from users.models import User
from users.serializers.user import UserSerializer


class UserUpdateDetailView(UpdateAPIView, RetrieveAPIView):
    """
    Manage user profile
    """

    serializer_class = UserSerializer
    permission_classes = (IsAuthenticated,)
    queryset = User.objects.all()

    def get_object(self):
        return self.request.user
