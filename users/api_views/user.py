from knox.models import AuthToken
from rest_framework import generics, status
from rest_framework.response import Response

from users.models import User
from users.permissions.user import OnlyUnathorized
from users.serializers.user import (LoginUserSerializer,
                                    UserSerializerRegistration)


class UserRegistration(generics.CreateAPIView):
    """
    User registration
    """

    serializer_class = UserSerializerRegistration
    permission_classes = [OnlyUnathorized]


class LoginAPI(generics.GenericAPIView):
    """
    User authentication
    :return auth token
    """

    serializer_class = LoginUserSerializer
    permission_classes = [OnlyUnathorized]

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        # _, token = AuthToken.objects.create(request.user)
        user = User.objects.filter(username=serializer.data["username"]).first()
        _, token = AuthToken.objects.create(user)
        return Response({"token": token}, status=status.HTTP_200_OK)
