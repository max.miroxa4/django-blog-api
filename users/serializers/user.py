from django.db import transaction
from rest_framework import serializers

from blog.serializers.post import SmallPostSerializer
from users.models import User


class UserSerializerRegistration(serializers.ModelSerializer):
    """
    User registration
    """

    password = serializers.CharField(required=True, min_length=8)

    class Meta:
        model = User
        fields = ("id", "username", "password", "first_name", "last_name")

    @transaction.atomic
    def create(self, validated_data):
        user = User.objects.create_user(
            username=validated_data.get("username"),
            email=validated_data.get("username"),
            password=validated_data.get("password"),
            first_name=validated_data.get("first_name", ""),
            last_name=validated_data.get("last_name", ""),
        )
        return user

    def validate(self, data):
        super().validate(data)
        if User.objects.filter(username=data.get("username")).exists():
            raise serializers.ValidationError(
                {"username": "This email is already taken"}
            )
        return data


class UserSerializer(serializers.ModelSerializer):
    posts = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = User
        exclude = ["groups", "user_permissions", "password"]
        read_only_fields = [
            "last_login",
            "is_staff",
            "is_active",
            "is_superuser",
            "date_joined",
            "username",
            "email",
        ]

    def get_posts(self, instance):
        qs = instance.posts.filter(is_draft=False)
        return SmallPostSerializer(qs, many=True, context=self.context).data


class LoginUserSerializer(serializers.Serializer):
    """
    Serializer for user authentication
    """

    username = serializers.CharField(required=True)
    password = serializers.CharField(required=True, min_length=8)

    def validate(self, data):
        user = User.objects.filter(username=data.get("username")).first()
        if user and user.check_password(data.get("password")):
            return user
        raise serializers.ValidationError("Username or password incorrect")
