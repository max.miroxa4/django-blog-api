from rest_framework import serializers

from users.models import User


class SmallUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        exclude = ["groups", "user_permissions", "password"]
        read_only_fields = [
            "last_login",
            "is_staff",
            "is_active",
            "is_superuser",
            "date_joined",
        ]
