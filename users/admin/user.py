from django.contrib import admin, auth

from users.models import User

__all__ = ("UserAdmin",)


@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    list_display = ["first_name", "last_name", "email", "username"]


admin.site.unregister(auth.models.Group)
