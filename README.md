# django api

### make .env file
cp .env-example .env

### install requirements
pip install -r requirements.txt

### migrate 
python manage.py migrate

### create super user

python manage.py createsuperuser


### run server

python manage.py runserver
