from django.core.mail import send_mail


def test():
    send_mail(
        "Subject here",
        "Here is the message.",
        "ilyshastebelskuy@gmail.com",
        ["i.stebelskuy@gmail.com"],
        fail_silently=False,
    )
