from django.db import models

from users.models import User


class Post(models.Model):
    title = models.CharField("Title", max_length=255)
    description = models.TextField("Description")
    user = models.ForeignKey(
        User, verbose_name="User", on_delete=models.CASCADE, related_name="posts"
    )
    image = models.FileField("Picture", upload_to="posts/images")
    is_draft = models.BooleanField("Is draft", default=True)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ("-created_at",)

    def __str__(self):
        return self.title[:30]
