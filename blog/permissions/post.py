from rest_framework import permissions

from blog.models import Post


class PostObjectPermissions(permissions.BasePermission):
    """
    Check if request user is creator
    """

    def has_permission(self, request, view):
        post = Post.objects.filter(id=view.kwargs.get("post_id")).first()
        return post.user == request.user if post else False
