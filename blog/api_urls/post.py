from django.urls import path

from blog.api_views.post import (DeletePostView, PostCreateView,
                                 PostUpdateView, RetrievePostView)

urlpatterns = [
    path("create/", PostCreateView.as_view(), name="post-create"),
    path("edit/<int:post_id>/", PostUpdateView.as_view(), name="post-edit"),
    path("get/<int:post_id>/", RetrievePostView.as_view(), name="post-get"),
    path("delete/<int:post_id>/", DeletePostView.as_view(), name="post-delete"),
]
