from django.urls import include, path

app_name = "api-blog"

urlpatterns = [path("posts/", include("blog.api_urls.post"))]
