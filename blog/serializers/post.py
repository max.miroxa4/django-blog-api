from rest_framework import serializers

from blog.models import Post
from users.serializers.profile import SmallUserSerializer


class PostSerializer(serializers.ModelSerializer):
    user = SmallUserSerializer(read_only=True)

    class Meta:
        model = Post
        fields = "__all__"


class SmallPostSerializer(serializers.ModelSerializer):
    class Meta:
        model = Post
        fields = "__all__"


class PostCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Post
        fields = ["title", "description", "is_draft", "image"]

    def create(self, validated_data):
        validated_data["user"] = self.context.get("request").user
        return super().create(validated_data)

    @property
    def data(self):
        return PostSerializer(instance=self.instance, context=self.context).data
