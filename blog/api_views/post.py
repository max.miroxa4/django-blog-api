from rest_framework import generics, permissions

from blog.models.post import Post
from blog.permissions.post import PostObjectPermissions
from blog.serializers.post import PostCreateSerializer


class PostCreateView(generics.CreateAPIView):
    """Create post"""

    serializer_class = PostCreateSerializer
    queryset = Post.objects.all().order_by("id")
    permission_classes = (permissions.IsAuthenticated,)


class PostUpdateView(generics.RetrieveUpdateAPIView):
    """
    Edit post
    """

    PERMS_LIST = []
    serializer_class = PostCreateSerializer
    queryset = Post.objects.all().order_by("id")
    permission_classes = [permissions.IsAuthenticated, PostObjectPermissions]
    lookup_url_kwarg = "post_id"


class RetrievePostView(generics.RetrieveAPIView):
    """
    Get detail post
    """

    PERMS_LIST = []
    serializer_class = PostCreateSerializer
    queryset = Post.objects.all().order_by("id")
    permission_classes = [permissions.IsAuthenticated]
    lookup_url_kwarg = "post_id"


class DeletePostView(generics.DestroyAPIView):
    """
    Delete post
    """

    PERMS_LIST = []
    serializer_class = PostCreateSerializer
    queryset = Post.objects.all().order_by("id")
    permission_classes = [permissions.IsAuthenticated, PostObjectPermissions]
    lookup_url_kwarg = "post_id"
