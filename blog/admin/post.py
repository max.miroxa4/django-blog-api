from django.contrib import admin

from blog.models import Post

__all__ = ("Post",)


@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
    list_display = ["id", "__str__", "user", "is_draft"]
    list_filter = ["is_draft"]
    readonly_fields = ["created_at"]
